import { token } from '../../../src/server';
import { rpcInject } from '../../../src/server/rpc';
import { useFoo } from './foo.service';

export interface FooMore {
  get(): string;
}

export const FooMore = token<FooMore>('FooMore');

export function fooMoreFactory () {
  const foo = useFoo();

  return {
    get () {
      return foo.get().toUpperCase();
    },
  };
}

export function useFooMore () {
  return rpcInject(FooMore);
}
