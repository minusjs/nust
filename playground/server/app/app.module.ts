import { defineModule } from '../../../src/server';
import { FooMore, fooMoreFactory } from './foo-more.service';
import { Foo, fooFactory } from './foo.service';

const app = defineModule({
  providers: [
    Foo.use(fooFactory),
    FooMore.use(fooMoreFactory),
  ],
  rpc: {
    preHandlers: [async () => {
      // eslint-disable-next-line no-console
      console.log('Pre handler from module');
    }],
  },
});
export default app;

export class FooError extends Error {}
