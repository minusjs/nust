import { defineFactoryClass, token } from '../../../src/server';
import { rpcInject } from '../../../src/server/rpc';

export interface Foo {
  get(): string;
  set(value: string): void;
}

export const Foo = token<Foo>('Foo');

export class FooService implements Foo {
  private value = 'bar';

  public get () {
    return this.value;
  }

  public set (value: string) {
    this.value = value;
  }
}

export const fooFactory = defineFactoryClass(FooService, []);

export function useFoo () {
  return rpcInject(Foo);
}
