import { errorMap } from '../../../src/server';
import app, { FooError } from '../app/app.module';
import { useFooMore } from '../app/foo-more.service';
import { useFoo } from '../app/foo.service';

export default app.rpcOptions({
  preHandlers: [
    async () => {
      // eslint-disable-next-line no-console
      console.log('Pre handler from namespace');
    },
  ],
  httpErrors: [
    errorMap(FooError, {
      status: 419,
      message: 'Teapot here !',
      metadata: {
        foo: 'bar',
      },
    }),
  ],
});

export const get = app.rpc(
  // eslint-disable-next-line no-console
  [() => console.log('Pre handler on func')],
  () => {
    return useFooMore().get();
  },
);

export const set = app.rpc(async (value: string) => {
  useFoo().set(value);
});

export const explode = app.rpc(async () => {
  throw new FooError();
});
