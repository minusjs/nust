import { defineBuildConfig } from 'unbuild';

export default defineBuildConfig({
  entries: [
    './src/module',
    {
      builder: 'mkdist',
      input: './src/server/',
      outDir: './dist/server',
    },
  ],
  declaration: true,
});
