import { addImports, addServerHandler, addTemplate, createResolver, defineNuxtModule } from '@nuxt/kit';
import fg from 'fast-glob';
import dedent from 'dedent';
import { createFilter } from '@rollup/pluginutils';
import * as path from 'pathe';

export interface ModuleOptions {
  rpcPatterns?: string | string[];
}

export default defineNuxtModule<ModuleOptions>({
  meta: {
    name: 'nust',
    configKey: 'nust',
  },
  defaults: {
    rpcPatterns: ['**/server/rpc/*.{ts,js,mjs}'],
  },
  async setup (options, nuxt) {
    const files: string[] = [];
    const filter = createFilter(options.rpcPatterns);

    const resolver = createResolver(import.meta.url);
    const clientPath = resolver.resolve(nuxt.options.buildDir, 'rpc');
    const handlerPath = resolver.resolve(nuxt.options.buildDir, 'rpc-server');
    const runtimeDir = resolver.resolve('./runtime');
    nuxt.options.build.transpile.push(runtimeDir, handlerPath, clientPath);

    nuxt.hook('builder:watch', async (e, path) => {
      if (e === 'change') { return; };
      if (filter(path)) {
        await scanRpcModules();
        await nuxt.callHook('builder:generateApp');
      }
    });

    addServerHandler({
      route: '/api/__rpc/:moduleId/:functionName',
      method: 'post',
      handler: handlerPath,
    });

    await scanRpcModules();

    addTemplate({
      filename: 'rpc-server.ts',
      write: true,
      getContents () {
        const filesWithId = files.map(file => ({
          file: file.replace(/\.ts$/, ''),
          id: getModuleId(file),
        }));

        return dedent`
          import { createRpcServer } from ${JSON.stringify(resolver.resolve(runtimeDir, 'server'))}
          ${filesWithId.map(i => `import * as ${i.id} from ${JSON.stringify(i.file)}`).join('\n')}

          export default createRpcServer({
            ${filesWithId.map(i => i.id).join(',\n')}
          })
        `;
      },
    });

    addTemplate({
      filename: 'rpc.ts',
      write: true,
      getContents () {
        const filesWithId = files.map(file => ({
          file: file.replace(/\.ts$/, ''),
          id: getModuleId(file),
        }));

        return dedent`
          import { createRpcClient, RpcConvert, RpcReturnType, RpcParameters } from ${JSON.stringify(resolver.resolve(runtimeDir, 'client'))};

          export type RpcMap = {
            ${filesWithId.map(i => `${i.id}: typeof import(${JSON.stringify(i.file)})`).join('\n')}
          };

          export const rpc = createRpcClient<RpcConvert<RpcMap>>();

          export type { RpcReturnType, RpcParameters };
          export { createRpcClient };
        `;
      },
    });

    addImports([
      {
        name: 'rpc',
        as: 'rpc',
        from: resolver.resolve(nuxt.options.buildDir, 'rpc'),
      },
      {
        name: 'RpcReturnType',
        as: 'RpcReturnType',
        type: true,
        from: resolver.resolve(nuxt.options.buildDir, 'rpc'),
      },
      {
        name: 'RpcParameters',
        as: 'RpcParameters',
        type: true,
        from: resolver.resolve(nuxt.options.buildDir, 'rpc'),
      },
    ]);

    async function scanRpcModules () {
      files.length = 0;
      const updatedFiles = await fg(options.rpcPatterns!, {
        cwd: nuxt.options.srcDir,
        absolute: true,
        onlyFiles: true,
        ignore: ['!**/node_modules', '!**/dist'],
      });
      files.push(...new Set(updatedFiles));
      return files;
    }
  },
});

export function getModuleId (file: string) {
  const id = path.basename(file, path.extname(file));
  const validId = id.replaceAll(/[^\p{L}\p{N}_$]/gu, '_').replace(/^\d/, '_$&');
  return camelize(validId);
}

function camelize (str: string) {
  return str.toLowerCase().replace(/[^a-zA-Z0-9]+(.)/g, (_, chr) => chr.toUpperCase());
}
