import { eventHandler, createError } from 'h3';
import type { RpcServerHandler, RpcServerHandlerOptions } from '@minusjs/nust/server';

type RpcFunctionsMap = Record<
  string,
  Record<string, RpcServerHandler<any> | any> & { default?: RpcServerHandlerOptions; }
>;

export function createRpcServer (rpcFunctions: RpcFunctionsMap) {
  return eventHandler((event) => {
    const { moduleId, functionName } =
      event.context.params || {} as Record<string, string>;

    if (!(moduleId in rpcFunctions)) {
      throw createError({
        statusCode: 400,
        statusMessage: `[nust]: Module ${moduleId as string} does not exist. Are you sure the file exists?`,
      });
    }

    if (typeof rpcFunctions[moduleId][functionName] === 'undefined') {
      throw createError({
        statusCode: 400,
        statusMessage: `[nust]: ${functionName as string} is not a function.`,
      });
    }

    const rpc: RpcServerHandler<any> = rpcFunctions[moduleId][functionName];
    return rpc.execute(event, rpcFunctions[moduleId].default);
  });
}
