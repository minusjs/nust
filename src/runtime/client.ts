import type { RpcServerHandler, RpcServerHandlerOptions } from '@minusjs/nust/server';

export type { RpcServerHandler };

export type KeysOfType<T, U> = {
  [K in keyof T]: T[K] extends U ? K : never;
}[keyof T];
type Simplify<T> = {[KeyType in keyof T]: T[KeyType]} & {};
type RemoveNever<T> = Simplify<Omit<T, KeysOfType<T, never>>>;

type OFetchOptions = Parameters<typeof globalThis.$fetch>[1];
type RpcFetchOptions = Omit<OFetchOptions, 'baseURL' | 'method' | 'body'>;
export type RpcFunctionOptions = RpcFetchOptions & {
  files?: Record<string, File>;
};

export interface RpcNamespace {
  [name: string]: RpcServerHandler<any> | RpcServerHandlerOptions;
};

export interface RpcFunction<
  Args extends Array<any> = any[],
  Return = any,
> {
  (...args: Args): Extract<Return, Promise<any>>;
  with(options: RpcFunctionOptions, ...args: Args):
    Extract<Return, Promise<any>>;
}

export type RpcConvert<M extends Record<string, RpcNamespace>> = Simplify<{
  [NKey in keyof M]: M[NKey] extends RpcNamespace
    ? RemoveNever<{
      [FKey in keyof M[NKey]]: FKey extends 'default'
        ? never
        : M[NKey][FKey] extends RpcServerHandler<any>
          ? RpcFunction<Parameters<M[NKey][FKey]['handler']>, ReturnType<M[NKey][FKey]['handler']>>
          : never;
    }>
    : {}
}>;

export type RpcParameters<T extends RpcFunction<any>> = Parameters<T>;
export type RpcReturnType<T extends RpcFunction<any>> =
  ReturnType<T> extends Promise<infer U> ? U : ReturnType<T>;

type RpcClientExtras = { $options: RpcFetchOptions; };
type RpcClient<T> = T & RpcClientExtras;

interface ProxyTargetParent {
  target: ProxyTarget;
  property: string;
}

interface ProxyTarget {
  (...args: any[]): Promise<any>;
  parent?: ProxyTargetParent;
  [name: string]: ProxyTarget | unknown;
}

export function createRpcClient<T> () {
  const client = (async () => {}) as ProxyTarget;
  const extras: RpcClientExtras = {
    $options: {},
  };

  const proxyHandler: ProxyHandler<ProxyTarget> = {
    get (target: ProxyTarget, property: string) {
      if (!target.parent && Object.keys(extras).includes(property)) {
        return extras[property as keyof RpcClientExtras];
      }

      const caller = <ProxyTarget>((...args: any[]): any => {
        const [modTarget, funcTarget, withTarget] = getCallerElements(caller);

        if (!funcTarget) { throw new Error('Cannot call module'); }

        let body: any = args;

        let options:RpcFunctionOptions = {};
        if (withTarget) {
          options = args[0];
          args.shift();

          if (options.files && Object.keys(options.files).length) {
            body = new FormData();
            body.append('args', JSON.stringify(args));
            for (const key in options.files) {
              body.append(key, options.files[key]);
            }
          }
        }

        return globalThis.$fetch(
          `/api/__rpc/${modTarget.property}/${funcTarget.property}`,
          {
            ...extras.$options,
            ...options,
            method: 'POST',
            body,
          });
      });

      caller.parent = { target, property };
      // eslint-disable-next-line no-return-assign
      return target[property] = new Proxy(caller, proxyHandler);
    },
  };

  return new Proxy(client, proxyHandler) as unknown as RpcClient<T>;
}

function getCallerElements (caller: ProxyTarget) {
  const parents: ProxyTargetParent[] = [caller.parent!];
  let parent = caller.parent;
  while (parent) {
    parent = parent.target.parent;
    if (parent) { parents.push(parent!); }
  }

  return parents.reverse();
}
