import { AsyncLocalStorage } from 'node:async_hooks';
import { readBody, readMultipartFormData, setResponseStatus, setResponseHeaders } from 'h3';
import type { H3Event, MultiPartData, EventHandler } from 'h3';
// eslint-disable-next-line import/no-named-as-default
import destr from 'destr';
import { FactoryFunction, FactoryInjects, InjectionToken, ProviderFactory } from './ioc';
import { Module } from './module';
import { HttpErrorsMap, HttpErrorsMapping, convertFromErrorsMap, errorToResponse } from './http-errors';

export interface RpcHandler<
  Args extends Array<any> = Array<any>,
  Return = any,
> {
  (...args: Args): Return | Promise<Return>;
}

export type RpcPreHandler = () => Promise<void> | void;

export interface RpcHandlerOptions {
  preHandlers?: RpcPreHandler[];
  httpErrors?: HttpErrorsMapping;
}

export class RpcFactory<T extends EventHandler = EventHandler>
  extends ProviderFactory<T> {
  constructor (
    public readonly resolve: FactoryFunction<FactoryInjects, T>,
    public readonly inject: FactoryInjects = {},
  ) {
    super(resolve, inject);
  }
}

interface RpcStorage {
  module: Module;
  event: H3Event;
  files: Record<string, MultiPartData>;
}

const rpcStorage = new AsyncLocalStorage<RpcStorage>();

export function useRpcStorage () {
  return rpcStorage.getStore()!;
}

export function useRpcEvent () {
  return useRpcStorage().event;
}

export function useRpcFiles () {
  return useRpcStorage().files;
}

export function rpcInject<T> (token: InjectionToken<T>): T {
  return useRpcStorage().module.get(token);
}

export class RpcServerHandler<Handler extends RpcHandler<any, any>> {
  constructor (
    public readonly module: Module,
    public readonly handler: Handler,
    public readonly options?: RpcHandlerOptions,
  ) {}

  public async execute (event: H3Event, ns?: RpcServerHandlerOptions) {
    await this.module.setup();

    let args: any[] = [];
    const files: Record<string, MultiPartData> = {};

    const multipartBody = await readMultipartFormData(event);
    if (multipartBody) {
      args = destr(multipartBody.find(part => part.name === 'args')!.data.toString('utf8'));
      for (const part of multipartBody) {
        if (part.type) { files[part.name!] = part; }
      }
    } else {
      args = (await readBody(event)) || [] as Array<any>;
    }

    return rpcStorage.run(
      { event, module: this.module, files },
      async () => {
        const { preHandlers, httpErrors } = this.mergeOptions(ns);

        try {
          for (const preHandler of preHandlers!) {
            await preHandler();
          }

          const output = await this.handler(...args);
          return output;
        } catch (error) {
          const response = this.handlerError(error, httpErrors);

          // TODO: make it configurable
          // eslint-disable-next-line no-constant-condition
          if (true) {
            response.body.stack = (error as Error).stack;
          }

          // TODO: hookable
          if (response.status >= 500) {
            // eslint-disable-next-line no-console
            console.error((error as Error).stack || error);
          }

          setResponseStatus(event, response.status, response.statusText);
          setResponseHeaders(event, response.headers);

          return response.body;
        }
      },
    );
  }

  private mergeOptions (ns?: RpcServerHandlerOptions): RpcHandlerOptions {
    return {
      preHandlers: [
        ...(this.module.options.rpc?.preHandlers || []),
        ...(ns?.options.preHandlers || []),
        ...(this.options?.preHandlers || []),
      ],
      httpErrors: [
        ...(this.module.options.rpc?.httpErrors || []),
        ...(ns?.options.httpErrors || []),
        ...(this.options?.httpErrors || []),
      ],
    };
  }

  private handlerError (error: any, httpErrors?: HttpErrorsMapping) {
    const errorsMap: HttpErrorsMap = new Map(httpErrors);
    const convertedError = convertFromErrorsMap(error, errorsMap);
    return errorToResponse(convertedError);
  }
}

export class RpcServerHandlerOptions {
  constructor (
    public readonly module: Module,
    public readonly options: RpcHandlerOptions,
  ) {}
}
