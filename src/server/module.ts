import { Container, ContainerOptions } from './ioc/container';
import { RpcHandler, RpcHandlerOptions, RpcPreHandler, RpcServerHandler, RpcServerHandlerOptions } from './rpc';

export interface ModuleOptions extends ContainerOptions {
  modules?: () => Promise<{ default: Module; }>[];
  rpc?: RpcHandlerOptions;
}

export class Module extends Container {
  private _isSetup = false;

  constructor (public readonly options: ModuleOptions = {}) {
    super(options);
  }

  public rpc<Args extends Array<any>, Return> (handler: RpcHandler<Args, Return>):
    RpcServerHandler<RpcHandler<Args, Return>>;

  public rpc<Args extends Array<any>, Return> (
    preHandlers: RpcPreHandler[],
    handler: RpcHandler<Args, Return>
  ): RpcServerHandler<RpcHandler<Args, Return>>;

  public rpc<Args extends Array<any>, Return> (
    options: RpcHandlerOptions,
    handler: RpcHandler<Args, Return>
  ): RpcServerHandler<RpcHandler<Args, Return>>;

  public rpc<Args extends Array<any>, Return>
  (...args: [RpcHandler] | [RpcPreHandler[], RpcHandler] | [RpcHandlerOptions, RpcHandler]):
    RpcServerHandler<RpcHandler<Args, Return>> {
    const handler = args.length === 1 ? args[0] : args[1];
    const options = args.length === 1
      ? {}
      : Array.isArray(args[0]) ? { preHandlers: args[0] } : args[0];

    return new RpcServerHandler(this, handler, options);
  }

  public rpcOptions (options: RpcHandlerOptions) {
    return new RpcServerHandlerOptions(this, options);
  }

  public async setup () {
    if (this._isSetup) { return; }
    if (this.options.modules) {
      const modules = await Promise.all(this.options.modules());
      for (const mod of modules) {
        this.registerImport(mod.default);
      }
    }
    this._isSetup = true;
  }
}

export function defineModule (options: ModuleOptions = {}): Module {
  return new Module(options);
}
