type Constructor<T, Arguments extends unknown[] = any[]> = new (
  ...args: Arguments
) => T;

export interface HttpErrorProps {
  status?: number;
  message?: string;
  metadata?: Record<string, unknown>;
}

export type HttpErrorValue = HttpErrorProps | ((err: any) => HttpErrorProps);

export class HttpError extends Error {
  constructor (
    public status = 500,
    public message = 'Internal Error',
    public metadata?: Record<string, unknown>,
  ) {
    super(message);
  }
}

export function httpError (
  status = 500,
  message = 'Internal Error',
  metadata: Record<string, unknown> = {},
): HttpError {
  return new HttpError(status, message, metadata);
}

export function isHttpError (error: unknown): boolean {
  return error instanceof HttpError;
}

function serializeHttpError (error: unknown): unknown {
  if (error instanceof Error) {
    const result: Record<string, unknown> = {
      message: error.message,
    };

    if (error instanceof HttpError && error.metadata) {
      for (const key in error.metadata) {
        const value = error.metadata[key];
        result[key] = value instanceof Error
          ? serializeHttpError(value)
          : value;
      }
    } else {
      for (const key in error) {
        result[key] = (error as any)[key];
      }
    }

    return result;
  }
  return error;
}

export type ErrorClass = Constructor<unknown>;
export type HttpErrorsMapping = Array<[ErrorClass, HttpErrorValue]>;
export type HttpErrorsMap = Map<ErrorClass, HttpErrorValue>;

export function errorMap (
  appError: ErrorClass,
  errorValue: HttpErrorValue,
): [ErrorClass, HttpErrorValue] {
  return [appError, errorValue];
}

export function convertFromErrorsMap (
  error: unknown,
  errorsMap?: HttpErrorsMap,

): HttpError | Error | any {
  if (!errorsMap) { return error; }

  const errorClass = Object.getPrototypeOf(error).constructor;
  const errorValue = errorsMap.get(errorClass);

  if (errorValue) {
    const { message, status, metadata } =
      typeof errorValue === 'function' ? errorValue(error) : errorValue;

    const newError = httpError(status, message, metadata || {});
    newError.stack = (error as Error).stack;
    return newError;
  }
  return error;
}

export interface HttpErrorResponse {
  status: number;
  statusText?: string;
  headers: Record<string, string>;

  body?: any;
}

export function errorToResponse (error: any): HttpErrorResponse {
  const res: HttpErrorResponse = {
    body: { error: 'Internal server error, please retry later' },
    status: 500,
    headers: {},
  };

  if (isHttpError(error)) {
    res.status = error.status;
    res.body = serializeHttpError(error);
    res.headers = error.headers || {};
  }

  res.headers!['content-type'] = 'application/json; charset=utf-8';

  return res;
}
