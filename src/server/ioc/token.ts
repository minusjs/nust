import { ProviderFactory } from './factory';
import { Provider } from './provider';

export class InjectionToken<T = unknown> {
  constructor (public readonly name: string) {}

  public use(factory: ProviderFactory<T> | (() => T)): Provider<T>;
  public use (factory: ProviderFactory<T> | (() => T)): Provider<T> {
    if (factory instanceof ProviderFactory) {
      return new Provider(this, factory);
    }
    return new Provider<T>(this, new ProviderFactory(factory));
  }

  public useValue (value: T) {
    return new Provider<T>(this, new ProviderFactory(() => value));
  }
}

export function token<T> (name: string): InjectionToken<T> {
  return new InjectionToken<T>(name);
}
