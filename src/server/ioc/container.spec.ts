import { describe, it, expect } from 'vitest';
import { Container } from './container';
import { defineFactory, defineFactoryClass, ProviderFactory } from './factory';
import { InjectionToken, token } from './token';
describe('Container', () => {
  it('can create a token', () => {
    interface Foo {
      bar(): string;
    }

    const foo = token<Foo>('Foo');
    expect(foo).toBeInstanceOf(InjectionToken);
  });

  it('can create and use a factory', () => {
    interface Service {
      execute(str: string): string;
    }
    const Service = token<Service>('Service');

    const serviceFactory = defineFactory(() => {
      return {
        execute (str): string {
          return str.toUpperCase();
        },
      } satisfies Service;
    });

    expect(serviceFactory).toBeInstanceOf(ProviderFactory);

    const service = serviceFactory.resolve({});
    expect(service.execute).toBeTypeOf('function');
    expect(service.execute('test')).toBe('TEST');
  });

  it('can create and use a class factory', () => {
    interface Service {
      execute(str: string): string;
    }
    const Service = token<Service>('Service');

    class MyService implements Service {
      execute (str: string): string {
        return str.toUpperCase();
      }
    }

    const serviceFactory = defineFactoryClass(MyService, []);

    const service = serviceFactory.resolve({});
    expect(service.execute).toBeTypeOf('function');
    expect(service.execute('test')).toBe('TEST');
  });

  it('can create container and resolve provider with injections', () => {
    interface Dep {
      execute(input: string): string;
    }
    const Dep = token<Dep>('Dep');

    interface Runner {
      run(input: string): string;
    }
    const Runner = token<Runner>('Runner');

    class RunnerImpl implements Runner {
      constructor (private readonly dep: Dep) {}

      run (input: string): string {
        return this.dep.execute(input);
      }
    }

    const runnerFactory = defineFactoryClass(RunnerImpl, [Dep]);

    const container = new Container({
      providers: [
        Dep.useValue({
          execute (input): string {
            return input.toUpperCase();
          },
        }),
        Runner.use(runnerFactory),
      ],
    });

    const runner = container.get(Runner);

    expect(runner).toBeDefined();
    expect(runner.run('test')).toBe('TEST');

    // Check is a singleton
    const another = container.get(Runner);
    expect(another).toBe(runner);

    // Can resolve a factory directly
    const resolvedFactory = container.resolveFactory(runnerFactory);
    expect(resolvedFactory).toBeDefined();
    expect(resolvedFactory.run('test')).toBe('TEST');
  });

  it('throws if provider does not exists', () => {
    interface Dep {
      execute(input: string): string;
    }
    const Dep = token<Dep>('Dep');

    const container = new Container();

    expect(() => {
      container.get(Dep);
    }).toThrow(`Unable to resolve token ${Dep.name}`);
  });

  it('throws if circular dependency detected', () => {
    const ServiceA = token<{ execute(): void; }>('ServiceA');
    const ServiceB = token<{ execute(): void; }>('ServiceB');

    const serviceAFactory = defineFactory({ service: ServiceB }, () => ({
      execute: (): undefined => undefined,
    }));
    const serviceBFactory = defineFactory({ service: ServiceA }, () => ({
      execute: (): undefined => undefined,
    }));

    const container = new Container({
      providers: [ServiceA.use(serviceAFactory), ServiceB.use(serviceBFactory)],
    });

    expect(() => {
      container.get(ServiceB);
    }).toThrow('Ciruclar dependencies detected [ServiceB > ServiceA > ServiceB]');
  });

  it('can resolve from imports', () => {
    interface Dep {
      execute(input: string): string;
    }
    const Dep = token<Dep>('Dep');

    const depContainer = new Container({
      providers: [
        Dep.useValue({
          execute (input): string {
            return input.toUpperCase();
          },
        }),
      ],
      exports: [Dep],
    });

    interface Runner {
      run(input: string): string;
    }
    const Runner = token<Runner>('Runner');

    class RunnerImpl implements Runner {
      constructor (private readonly dep: Dep) {}

      run (input: string): string {
        return this.dep.execute(input);
      }
    }

    const runnerFactory = defineFactoryClass(RunnerImpl, [Dep]);

    const runnerContainer = new Container({
      imports: [depContainer],
      providers: [Runner.use(runnerFactory)],
    });

    const runner = runnerContainer.get(Runner);

    expect(runner).toBeDefined();
    expect(runner.run('test')).toBe('TEST');
  });
});
