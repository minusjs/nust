import { ProviderFactory } from './factory';
import { InjectionToken } from './token';

export class Provider<T = unknown> {
  public value?: T;

  constructor (
    public readonly token: InjectionToken<T>,
    public readonly factory: ProviderFactory<T>,
  ) {}

  get isResolved (): boolean {
    return !!this.value;
  }

  resolve (dependencies: Record<string, unknown>): T {
    if (this.value) { return this.value; }
    this.value = this.factory.resolve(dependencies) as T;
    return this.value;
  }
}
